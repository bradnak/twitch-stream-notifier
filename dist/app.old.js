"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const Axios = require("axios");
const fs = require("fs");
const Discord = require("discord.js");
const TwitchAuth = require("twitch-auth");
const TwitchChat = require("twitch-chat-client");
class TwitchStreamNotifier {
    constructor() {
        this.http = express();
        this.client = Axios.default.create();
        this.secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
        this.discord = new Discord.Client();
        this.streamMessages = [];
        this.chat = new TwitchChat.ChatClient(new TwitchAuth.RefreshableAuthProvider(new TwitchAuth.StaticAuthProvider(this.secrets.twitch.client_id), {
            clientSecret: this.secrets.twitch.client_secret,
            refreshToken: this.secrets.twitch.tokens.chat.refresh_token
        }));
        this.clientoptions = {
            headers: {
                "Client-ID": this.secrets.twitch.client_id,
                "Authorization": `Bearer ${this.secrets.twitch.tokens.app.access_token}`
            }
        };
        this.suboptions = {
            headers: {
                "Client-ID": this.secrets.twitch.client_id,
                "Authorization": `Bearer ${this.secrets.twitch.tokens.sub.access_token}`
            }
        };
        this.refreshToken = () => {
            this.client.post(`https://id.twitch.tv/oauth2/token?grant_type=refresh_token&client_id=${this.secrets.twitch.client_id}&client_secret=${this.secrets.twitch.client_secret}&refresh_token=${this.secrets.twitch.tokens.chat.refresh_token}`)
                .then((res) => {
                this.client.post(`https://id.twitch.tv/oauth2/token?grant_type=refresh_token&client_id=${this.secrets.twitch.client_id}&client_secret=${this.secrets.twitch.client_secret}&refresh_token=${this.secrets.twitch.tokens.sub.refresh_token}`)
                    .then((subres) => {
                    let newToken = {
                        app: this.secrets.twitch.tokens.app,
                        chat: {
                            "access_token": res.data.access_token,
                            "refresh_token": res.data.refresh_token,
                            "scope": res.data.scope,
                            "token_type": res.data.token_type
                        },
                        sub: {
                            "access_token": subres.data.access_token,
                            "refresh_token": subres.data.refresh_token,
                            "scope": subres.data.scope,
                            "token_type": subres.data.token_type
                        }
                    };
                    let secrets = {
                        "discord": this.secrets.discord,
                        "twitch": {
                            "client_id": this.secrets.twitch.client_id,
                            "client_secret": this.secrets.twitch.client_secret,
                            "tokens": newToken
                        }
                    };
                    this.secrets = secrets;
                    fs.writeFile("lib/secrets.json", JSON.stringify(secrets, null, 4), (err) => {
                        if (err) {
                            console.error(`Unable to write new token to file: ${err}`);
                        }
                        else {
                            console.log("Twitch token refreshed.");
                            new TwitchChat.ChatClient(new TwitchAuth.RefreshableAuthProvider(new TwitchAuth.StaticAuthProvider(this.secrets.twitch.client_id), {
                                clientSecret: this.secrets.twitch.client_secret,
                                refreshToken: this.secrets.twitch.tokens.chat.refresh_token
                            }));
                        }
                    });
                })
                    .catch((err) => {
                    console.error(`Unable to refresh sub token: ${err}`);
                });
            })
                .catch((err) => {
                console.error(`Unable to refresh chat token: ${err}`);
            });
        };
        this.http.use(express.json());
        this.refreshToken();
        setInterval(this.refreshToken, 10800 * 1000);
        this.client.get("https://api.twitch.tv/helix/streams?user_id=56361303", this.clientoptions).then((res) => {
            if (res.data.data.length > 0) {
                this.streamup = true;
            }
            else {
                this.streamup = false;
            }
            console.log(`Stream up var is set to: ${this.streamup}`);
        }).catch(err => {
            console.error(`Unable to get stream status, assuming offline: ${err}`);
            this.streamup = false;
        });
        this.client.get("https://api.twitch.tv/helix/users/follows?first=1&to_id=56361303", this.clientoptions).then((res) => {
            this.totalFollowers = res.data.total;
        }).catch(err => {
            console.error(`Unable to determine the number of followers, defaulting to 0: ${err}`);
            this.totalFollowers = 0;
        });
        this.discord.on('ready', () => {
            console.log(`Logged into Discord as ${this.discord.user.tag}`);
            this.alertChannel = this.discord.guilds.cache.get("93155159563075584").channels.cache.get("424717288860811265");
            this.streamChannel = this.discord.guilds.cache.get("93155159563075584").channels.cache.get("424717288860811265");
        });
        this.http.post('/stream', (req, res) => {
            res.status(200);
            if (!this.streamup && req.body.data[0]) {
                this.streamup = true;
                console.log("Stream has started.");
                this.title = req.body.data[0].title;
                this.gameID = req.body.data[0].game_id;
                this.client.get(`https://api.twitch.tv/helix/games?id=${req.body.data[0].game_id}`, this.clientoptions).then((gameres) => {
                    const embed = new Discord.MessageEmbed();
                    embed.setTitle(`${req.body.data[0].user_name} just went live!`)
                        .setURL(`https://www.twitch.tv/${req.body.data[0].user_name.toLowerCase()}`)
                        .setColor(9520895)
                        .setTimestamp(req.body.data[0].started_at)
                        .setFooter("Twitch", "https://static.twitchcdn.net/assets/favicon-32-d6025c14e900565d6177.png")
                        .setThumbnail('https://static-cdn.jtvnw.net/jtv_user_pictures/804f89bc-6688-4dd8-8a57-a27ebeb832ab-profile_image-70x70.png')
                        .addFields([{ name: "Title", value: `**${req.body.data[0].title}**` }, { name: "Playing", value: gameres.data.data[0].name }]);
                    this.streamChannel.send(`@everyone DrGluon is live playing ${gameres.data.data[0].name}: ${req.body.data[0].title}`, embed)
                        .then((message) => {
                        console.log(`Sucessfully sent message ID ${message.id}`);
                        this.streamMessages.push(message.id);
                    }).catch((err) => {
                        console.error(`Error sending announcement message: ${err}`);
                    });
                });
            }
            else if (this.streamup && !req.body.data[0]) {
                this.streamup = false;
                this.gameID = null;
                this.title = null;
                console.log("Stream has ended");
                this.streamMessages.forEach((message) => {
                    this.streamChannel.messages.cache.get(message).delete();
                });
                this.streamMessages = [];
            }
            else if (this.streamup && req.body.data[0]) {
                console.log("Stream info changed mid-broadcast.");
                if (req.body.data[0].title != this.title || req.body.data[0].game_id != this.gameID) {
                    this.title = req.body.data[0].title;
                    this.gameID = req.body.data[0].game_id;
                    this.client.get(`https://api.twitch.tv/helix/games?id=${req.body.data[0].game_id}`, this.clientoptions).then((gameres) => {
                        const embed = new Discord.MessageEmbed();
                        embed.setTitle("Stream info changed!")
                            .setURL(`https://www.twitch.tv/${req.body.data[0].user_name.toLowerCase()}`)
                            .setColor(9520895)
                            .setTimestamp(new Date())
                            .setFooter("Twitch", "https://static.twitchcdn.net/assets/favicon-32-d6025c14e900565d6177.png")
                            .setThumbnail('https://static-cdn.jtvnw.net/jtv_user_pictures/804f89bc-6688-4dd8-8a57-a27ebeb832ab-profile_image-70x70.png')
                            .addFields([{ name: "Title", value: `**${req.body.data[0].title}**` }, { name: "Playing", value: gameres.data.data[0].name }, { name: "Viewers", value: req.body.data[0].viewer_count }]);
                        this.streamChannel.send(embed).then((message) => {
                            console.log(`Sucessfully sent message ID ${message.id}`);
                            this.streamMessages.push(message.id);
                        }).catch(err => {
                            console.error(`Error sending info change embed: ${err}`);
                        });
                    });
                }
            }
            else {
                console.error("Unknown error");
            }
        }).get('/stream', (req, res) => {
            res.send(req.query['hub.challenge']);
            console.log("Stream subscription verification sent.");
        }).post('/follow', (req, res) => {
            res.status(200);
            console.log("New follower event received");
            if (req.body.total === this.totalFollowers) {
                return;
            }
            else {
                this.alertChannel.send(`**New Follower**: ${req.body.data[0].from_name}`)
                    .then((message) => {
                    console.log(`New follower announced: ${message.id}`);
                })
                    .catch((err) => {
                    console.error(`Error announcing new follower in Discord: ${err}`);
                });
            }
            this.totalFollowers = req.body.total;
        }).get('/follow', (req, res) => {
            res.send(req.query['hub.challenge']);
            console.log("Follow subscription verification sent.");
        }).post('/sub', (req, res) => {
            res.status(200);
            console.log("New subscriber event received");
            let response;
            if (req.body.data[0].event_data.is_gift) {
                switch (req.body.data[0].event_data.tier) {
                    case '1000':
                        response = `**New Tier 1 Gift Sub** from ${req.body.data[0].event_data.gifter_name} to ${req.body.data[0].event_data.user_name}`;
                        break;
                    case '2000':
                        response = `**New Tier 2 Gift Sub** from ${req.body.data[0].event_data.gifter_name} to ${req.body.data[0].event_data.user_name}`;
                        break;
                    case '3000':
                        response = `**New Tier 3 Gift Sub** from ${req.body.data[0].event_data.gifter_name} to ${req.body.data[0].event_data.user_name}`;
                        break;
                }
            }
            else {
                switch (req.body.data[0].event_data.tier) {
                    case 'prime':
                        response = `**New Prime Subscriber**: ${req.body.data[0].event_data.user_name}`;
                        break;
                    case '1000':
                        response = `**New Tier 1 Subscriber**: ${req.body.data[0].event_data.user_name}`;
                        break;
                    case '2000':
                        response = `**New Tier 2 Subscriber**: ${req.body.data[0].event_data.user_name}`;
                        break;
                    case '3000':
                        response = `**New Tier 3 Subscriber**: ${req.body.data[0].event_data.user_name}`;
                        break;
                }
            }
            this.alertChannel.send(response).then((message) => {
                console.log(`New Subscriber announced: ${message.id}`);
            }).catch((err) => {
                console.error(`Error posting subscriber message: ${err}`);
            });
        }).listen(8000, () => {
            console.log('Stream Notifier Server started on port 8000');
            this.setSubs();
            setInterval(this.setSubs, 86400 * 1000);
            this.discord.login(this.secrets.discord.token);
            this.chat.connect().then(() => {
                this.chat.join("drgluon").then(() => {
                    this.chat.onHosted((channel, byChannel, auto, viewers) => {
                        console.log("Host event received");
                        if (viewers >= 5) {
                            this.alertChannel.send(`**Hosted** by **${byChannel}** for ${viewers} viewers`)
                                .then((message) => {
                                console.log(`New host announced: ${message.id}`);
                            })
                                .catch((err) => {
                                console.error(`Error announcing new host: ${err}`);
                            });
                        }
                    });
                }).catch(err => {
                    console.error(`Unable to join channel: ${err}`);
                });
            }).catch(err => {
                console.error(`Unable to connect to chat: ${err}`);
            });
        });
    }
    setSubs() {
        this.client.post('https://api.twitch.tv/helix/webhooks/hub', {
            "hub.callback": "http://ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com:8000/stream",
            "hub.mode": "subscribe",
            "hub.topic": "https://api.twitch.tv/helix/streams?user_id=56361303",
            "hub.lease_seconds": 86400
        }, this.clientoptions).then((_res) => {
            console.log("Stream subscription sent sucessfully");
        }).catch((err) => {
            console.error(`Error subscribing to stream notifications: ${err}`);
        });
        this.client.post('https://api.twitch.tv/helix/webhooks/hub', {
            "hub.callback": "http://ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com:8000/follow",
            "hub.mode": "subscribe",
            "hub.topic": "https://api.twitch.tv/helix/users/follows?first=1&to_id=56361303",
            "hub.lease_seconds": 86400
        }, this.clientoptions).then((_res) => {
            console.log("Follow subscription sent sucessfully");
        }).catch((err) => {
            console.error(`Error subscribing to follow notifications: ${err}`);
        });
        this.client.post('https://api.twitch.tv/helix/webhooks/hub', {
            "hub.callback": "http://ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com:8000/sub",
            "hub.mode": "subscribe",
            "hub.topic": "https://api.twitch.tv/helix/subscriptions/events?broadcaster_id=56361303&first=1",
            "hub.lease_seconds": 86400
        }, this.suboptions).then((_res) => {
            console.log("Subscriber subscription sent successfully");
        }).catch((err) => {
            console.error(`Error subscribing to subscriber notifications: ${err}`);
        });
    }
    ;
}
exports.default = new TwitchStreamNotifier();
//# sourceMappingURL=app.old.js.map