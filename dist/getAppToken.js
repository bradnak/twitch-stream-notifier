"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const TwitchAuth = require("twitch-auth");
class getAppToken {
    constructor() {
        this.secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
        TwitchAuth.getAppToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret).then((appToken) => {
            let diff = appToken.expiryDate.getTime() - Date.now();
            let expires_in = Math.abs(diff / 1000);
            this.secrets.twitch.tokens.app = {
                access_token: appToken.accessToken,
                expires_in: expires_in,
                refresh_token: appToken.refreshToken,
                scope: appToken.scope
            };
            fs.writeFileSync('lib/secrets.json', JSON.stringify(this.secrets));
        }).catch(err => {
            console.error(`Unable to get an app token: ${err}`);
        });
    }
}
exports.default = new getAppToken();
;
//# sourceMappingURL=getAppToken.js.map