"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const fs = require("fs");
const Twitch = require("twitch");
const TwitchAuth = require("twitch-auth");
const TwitchChat = require("twitch-chat-client");
const TwitchWebhooks = require("twitch-webhooks");
class TwitchStreamNotifier {
    constructor() {
        this.port = 8000;
        this.secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
        this.discord = new Discord.Client();
        this.webhookAuth = new TwitchAuth.StaticAuthProvider(this.secrets.twitch.client_id, new Twitch.AccessToken(this.secrets.twitch.tokens.app));
        this.apiClient = new Twitch.ApiClient({ authProvider: this.webhookAuth });
        this.chatAuth = new TwitchAuth.RefreshableAuthProvider(new TwitchAuth.StaticAuthProvider(this.secrets.twitch.client_id, new Twitch.AccessToken(this.secrets.twitch.tokens.chat)), {
            clientSecret: this.secrets.twitch.client_secret,
            refreshToken: this.secrets.twitch.tokens.chat.refresh_token,
            onRefresh: (token) => {
                this.secrets.twitch.tokens.chat = {
                    access_token: token.accessToken,
                    refresh_token: token.refreshToken,
                    scope: token.scope
                };
                fs.writeFileSync("lib/secrets.json", JSON.stringify(this.secrets, null, 4));
            }
        });
        this.twitchchannel = "drgluon";
        this.chat = new TwitchChat.ChatClient(this.chatAuth, { channels: [this.twitchchannel] });
        this.listener = new TwitchWebhooks.WebHookListener(this.apiClient, new TwitchWebhooks.SimpleAdapter({
            hostName: 'ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com',
            listenerPort: this.port
        }));
        this.streamMessages = [];
        this.apiClient.helix.streams.getStreamByUserId("56361303").then((stream) => {
            if (stream) {
                this.streamup = true;
            }
            else {
                this.streamup = false;
            }
            console.log(`The stream is ${this.streamup ? 'live' : 'offline'}`);
        }).catch(err => {
            console.error(`Error getting stream status: ${err}`);
        });
        this.apiClient.helix.users.getUserById('56361303').then((user) => {
            this.avatarURL = user.profilePictureUrl;
        }).catch(err => {
            console.error(`Unable to grab user data: ${err}`);
        });
        this.discord.on('ready', () => {
            console.log(`Logged into Discord as ${this.discord.user.tag}`);
            this.alertChannel = this.discord.guilds.cache.get("93155159563075584").channels.cache.get("159939795856916480");
            this.streamChannel = this.discord.guilds.cache.get("93155159563075584").channels.cache.get("785363356751888394");
        });
        this.discord.on('message', (message) => {
            if (message.channel === this.alertChannel && message.content === '!resetnotifier') {
                message.reply("Resetting Chat Alerts...");
                this.chat.quit().then(() => {
                    console.log("Successfully disconnected from chat.");
                }).catch((err) => {
                    console.error(`Unable to cleanly disconnect from chat: ${err}`);
                }).finally(() => {
                    this.chat = new TwitchChat.ChatClient(this.chatAuth, { channels: [this.twitchchannel] });
                    this.setChatHandlers();
                    console.log("Reconnected to chat.");
                    message.reply("Successully reconnected to chat");
                });
            }
        });
        this.listener.subscribeToStreamChanges('56361303', (stream) => {
            if (!this.streamup && stream) {
                console.log("Stream has started");
                this.alertChannel.send("---- Stream started ---").then((message) => {
                    console.log(`Marked stream start in alert channel: ${message.id}`);
                }).catch(err => {
                    console.error(`Unable to post stream start in alert channel: ${err}`);
                });
                this.streamup = true;
                this.title = stream.title;
                this.gameID = stream.gameId;
                stream.getGame().then((game) => {
                    let embed = new Discord.MessageEmbed();
                    embed.setTitle(`${stream.userDisplayName} just went live!`)
                        .setURL(`https://www.twitch.tv/${stream.userDisplayName.toLowerCase()}`)
                        .setColor(9520895)
                        .setTimestamp(stream.startDate)
                        .setFooter("Twitch", "https://static.twitchcdn.net/assets/favicon-32-d6025c14e900565d6177.png")
                        .setThumbnail(this.avatarURL)
                        .addFields([{ name: "Title", value: `**${this.title}**` }, { name: "Playing", value: game.name }]);
                    this.streamChannel.send(`<@&862510976950730812> ${stream.userDisplayName} is live playing ${game.name}: ${stream.title}`, { "embed": embed, "allowedMentions": { "parse": ["roles"] } }).then((message) => {
                        console.log(`Successfully sent stream announcement, ID: ${message.id}`);
                        this.streamMessages.push(message.id);
                    }).catch(err => {
                        console.error(`Error sending stream announcement: ${err}`);
                    });
                }).catch(err => {
                    console.error(`Unable to get game data: ${err}`);
                });
            }
            else if (this.streamup && !stream) {
                this.alertChannel.send('---- Stream Ended ----').then((message) => {
                    console.log(`Marked stream end in alert channel: ${message.id}`);
                }).catch(err => {
                    console.error(`Unable to post stream end in alert channel: ${err}`);
                });
                this.streamup = false;
                this.gameID = null;
                this.title = null;
                console.log("Stream has ended");
                this.streamMessages.forEach((message) => {
                    this.streamChannel.messages.cache.get(message).delete({ reason: "Stream went offline" });
                });
                this.streamMessages = [];
            }
            else if (this.streamup && stream) {
                console.log("Stream info changed mid-broadcast");
                if (stream.title !== this.title || stream.gameId !== this.gameID) {
                    this.title = stream.title;
                    this.gameID = stream.gameId;
                    stream.getGame().then((game) => {
                        let embed = new Discord.MessageEmbed();
                        embed.setTitle("Stream info changed!")
                            .setURL(`https://www.twitch.tv/${stream.userDisplayName.toLowerCase()}`)
                            .setColor(9520895)
                            .setTimestamp(new Date())
                            .setFooter("Twitch", "https://static.twitchcdn.net/assets/favicon-32-d6025c14e900565d6177.png")
                            .setThumbnail(this.avatarURL)
                            .addFields([{ name: "Title", value: `**${stream.title}**` }, { name: "Playing", value: game.name }, { name: "Viewers", value: stream.viewers }]);
                        this.streamChannel.send(embed).then((message) => {
                            console.log(`Sucessfully sent stream change embed, ID: ${message.id}`);
                            this.streamMessages.push(message.id);
                        }).catch(err => {
                            console.error(`Error sending stream change embed: ${err}`);
                        });
                    });
                }
            }
            else {
                console.error("Stream webhook was activated, but we're in an invalid state.");
            }
        }).then((sub) => {
            console.log(`Successfully subscribed to stream alerts, ID: ${sub.id}`);
        }).catch(err => {
            console.error(`Error subscribing to stream alerts: ${err}`);
        });
        this.listener.subscribeToFollowsToUser('56361303', (follow) => {
            this.alertChannel.send(`**New Follower**: \`${follow.userDisplayName}\``).then((message) => {
                console.log(`New follower announced: ${message.id}`);
            }).catch(err => {
                console.error(`Error announcing new follower in Discord: ${err}`);
            });
        }).then((sub) => {
            console.log(`Subscribed to Follow alerts, ID: ${sub.id}`);
        }).catch(err => {
            console.error(`Unable to subscribe to follow alerts: ${err}`);
        });
        this.listener.listen().then(() => {
            console.log(`Listening for webhook notifications on port ${this.port}`);
        }).catch(err => {
            console.error(`Error listening for notifications: ${err}`);
        });
        this.setChatHandlers();
        this.discord.login(this.secrets.discord.token);
        process.on("SIGINT", () => {
            this.chat.quit().then(() => {
                console.log("Closed connection to chat");
                this.listener.unlisten().then(() => {
                    console.log("Webhook listener stopped");
                    this.discord.destroy();
                    process.exit(0);
                }).catch(err => {
                    console.error(`Unable to stop Webhook listener: ${err}`);
                    process.exit(1);
                });
            }).catch(err => {
                console.error(`Unable to close connection to chat: ${err}`);
                process.exit(2);
            });
        });
    }
    getTier(plan) {
        switch (plan) {
            case '1000':
                return 1;
            case `2000`:
                return 2;
            case '3000':
                return 3;
            default:
                console.error("Can not determine Tier number, returning 0");
                return 0;
        }
    }
    setChatHandlers() {
        this.chat.onJoin((channel, user) => {
            console.log(`${user} joined channel ${channel}`);
        });
        this.chat.onDisconnect((manually, reason) => {
            if (manually) {
                console.log(`Manually disconnected from Twitch chat: ${reason}`);
            }
            else {
                console.log(`Disconnected from Twitch chat: ${reason}`);
                this.chat.connect().then(() => {
                    console.log("Reconnected to Twitch chat");
                }).catch(err => {
                    console.error(`Error reconnecting to Twitch chat: ${err}`);
                });
            }
        });
        this.chat.onHosted((channel, byChannel, auto, viewers) => {
            if (!auto && viewers > 5) {
                this.alertChannel.send(`**Hosted** by \`${byChannel}\` for ${viewers} viewers`).then((message) => {
                    console.log(`Host announced: ${message.id}`);
                }).catch(err => {
                    console.error(`Error posting host message: ${err}`);
                });
            }
        });
        this.chat.onRaid((channel, user, raidInfo, msg) => {
            this.alertChannel.send(`**Raided** by \`${raidInfo.displayName}\` for ${raidInfo.viewerCount} viewers`).then((message) => {
                console.log(`Raid announced: ${message.id}`);
            }).catch(err => {
                console.error(`Error posting raid message: ${err}`);
            });
        });
        this.chat.onSub((channel, user, subInfo, _raw) => {
            let response;
            if (subInfo.isPrime) {
                if (subInfo.months > 1) {
                    if (subInfo.streak) {
                        response = `**Prime-Subscription**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                    }
                    else {
                        response = `**Prime Re-Subscription**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months)`;
                    }
                }
                else {
                    response = `**New Prime Subscriber**: \`\`${subInfo.displayName}\`\``;
                }
            }
            else {
                let tier = this.getTier(subInfo.plan);
                if (subInfo.months === 1) {
                    if (tier === 0) {
                        response = `**New Subscriber*: \`\`${subInfo.displayName}\`\``;
                    }
                    else {
                        response = `**New Tier ${tier} Subscriber**: \`\`${subInfo.displayName}\`\``;
                    }
                }
                else {
                    if (subInfo.streak) {
                        if (tier === 0) {
                            response = `**Re-Subscriber** : \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                        }
                        else {
                            response = `**Tier ${tier} Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                        }
                    }
                    else {
                        if (tier === 0) {
                            response = `**Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months)`;
                        }
                        else {
                            response = `**Tier ${tier} Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months`;
                        }
                    }
                }
            }
            this.alertChannel.send(response).then((subMessage) => {
                console.log(`New subscriber announced: ${subMessage.id}`);
            }).catch(err => {
                console.error(`Error posting subscriber message: ${err}`);
            });
        });
        this.chat.onResub((channel, user, subInfo, raw) => {
            let response;
            if (subInfo.isPrime) {
                if (subInfo.streak) {
                    response = `**Prime Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                }
                else {
                    response = `**Prime Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months)`;
                }
            }
            else {
                let tier = this.getTier(subInfo.plan);
                if (subInfo.streak) {
                    if (tier === 0) {
                        response = `**Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                    }
                    else {
                        response = `**Tier ${tier} Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months, ${subInfo.streak}-month streak)`;
                    }
                }
                else {
                    if (tier === 0) {
                        response = `**Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months)`;
                    }
                    else {
                        response = `**Tier ${tier} Re-Subscriber**: \`\`${subInfo.displayName}\`\` (${subInfo.months} months)`;
                    }
                }
            }
            this.alertChannel.send(response).then((resubMessage) => {
                console.log(`Re-subscription announced: ${resubMessage.id}`);
            }).catch(err => {
                console.error(`Error posting re-subscription message: ${err}`);
            });
        });
        this.chat.onCommunitySub((channel, user, subInfo, raw) => {
            let tier = this.getTier(subInfo.plan);
            let response;
            if (subInfo.gifterDisplayName === undefined || subInfo.gifterDisplayName === 'undefined') {
                subInfo.gifterDisplayName = 'An anonymous gifter';
            }
            if (tier === 0) {
                response = `**Community Sub bomb**: \`${subInfo.gifterDisplayName}\` gifted ${subInfo.count} subs`;
            }
            else {
                response = `**Community Sub bomb**: \`${subInfo.gifterDisplayName}\` gifted ${subInfo.count} Tier ${tier} subs`;
            }
            this.alertChannel.send(response).then((commSubMsg) => {
                console.log(`Community Sub announced: ${commSubMsg.id}`);
            }).catch(err => {
                console.error(`Error posting Community Gift message: ${err}`);
            });
        });
        this.chat.onGiftPaidUpgrade((channel, user, subInfo, raw) => {
            let tier = this.getTier(subInfo.plan);
            let response;
            if (tier === 0) {
                response = `**Paid Gift Sub upgrade**: \`\`${subInfo.displayName}\`\` is continuing the Gift Sub they got from \`${subInfo.gifterDisplayName}\``;
            }
            else {
                response = `**Paid Gift Sub upgrade**: \`\`${subInfo.displayName}\`\` is continuing the Tier ${tier} Gift Sub they got from \`${subInfo.gifterDisplayName}\``;
            }
            this.alertChannel.send(response).then((giftUpgradeMsg) => {
                console.log(`Paid Gift upgrade announced: ${giftUpgradeMsg.id}`);
            }).catch(err => {
                console.error(`Error posting paid gift upgrade message: ${err}`);
            });
        });
        this.chat.onPrimePaidUpgrade((channel, user, subInfo, msg) => {
            let tier = this.getTier(subInfo.plan);
            let response;
            if (tier === 0) {
                response = `**Prime Upgrade**: \`\`${subInfo.displayName}\`\``;
            }
            else {
                response = `**Prime Upgrade**: \`\`${subInfo.displayName}\`\` to Tier ${tier}`;
            }
            this.alertChannel.send(response).then((PrimeUpgradeMsg) => {
                console.log(`Prime upgrade announced: ${PrimeUpgradeMsg.id}`);
            }).catch(err => {
                console.error(`Error posting prime upgrade message: ${err}`);
            });
        });
        this.chat.connect().then(() => {
            console.log("Connected to Chat server");
        }).catch(err => {
            console.error(`Unable to connect to chat server: ${err}`);
        });
    }
}
exports.default = new TwitchStreamNotifier();
//# sourceMappingURL=app.js.map