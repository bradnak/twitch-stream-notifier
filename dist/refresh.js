"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const TwitchAuth = require("twitch-auth");
class RefreshTokens {
    constructor() {
        this.secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
        console.log("Refreshing Stream Notifier tokens...");
        TwitchAuth.refreshUserToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret, this.secrets.twitch.tokens.chat.refresh_token).then((chatToken) => {
            console.log("Chat token received");
            this.secrets.twitch.tokens.chat.access_token = chatToken.accessToken;
            this.secrets.twitch.tokens.chat.refresh_token = chatToken.refreshToken;
            this.secrets.twitch.tokens.chat.scope = chatToken.scope;
            console.log("Chat token saved");
            TwitchAuth.refreshUserToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret, this.secrets.twitch.tokens.sub.refresh_token).then((subToken) => {
                console.log("Sub token received");
                this.secrets.twitch.tokens.sub.access_token = subToken.accessToken;
                this.secrets.twitch.tokens.sub.refresh_token = subToken.refreshToken;
                this.secrets.twitch.tokens.sub.scope = chatToken.scope;
                console.log("Sub token saved");
                fs.writeFileSync("lib/secrets.json", JSON.stringify(this.secrets, null, 4));
            }).catch(err => {
                console.error(`Unable to refresh sub token: ${err}`);
            });
        }).catch(err => {
            console.error(`Unable to refresh chat token: ${err}`);
        });
    }
}
exports.default = new RefreshTokens();
//# sourceMappingURL=refresh.js.map