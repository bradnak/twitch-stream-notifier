"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const fs = require("fs");
class MessageDeleter {
    constructor() {
        this.token = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8")).discord.token;
        this.discord = new Discord.Client();
        this.discord.on("ready", () => {
            let channel = this.discord.guilds.cache.get('93155159563075584').channels.cache.get('424717288860811265');
            channel.messages.fetch().then((messages) => {
                messages.forEach((message) => {
                    message.delete().then((deletedMsg) => {
                        console.log(`Message deleted: ${deletedMsg.id}`);
                    }).catch(err => {
                    });
                });
            }).catch(err => {
            });
        });
    }
}
exports.default = new MessageDeleter();
//# sourceMappingURL=messagedeleter.js.map