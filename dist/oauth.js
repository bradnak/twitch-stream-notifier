"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Axios = require("axios");
const Express = require("express");
const fs = require("fs");
class NotifierOauth {
    constructor() {
        this.secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
        this.http = Express();
        this.port = 8080;
        this.redirect_uri = "http://ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com:8080/callback";
        this.webclient = Axios.default.create();
        this.http.get('/', (req, res) => {
            res.send("Welcome to Bradnak Stream Notifier. To login, visit /authorize/chat or /authorize/webhook");
        }).get('/authorize/chat', (req, res) => {
            res.redirect(`https://id.twitch.tv/oauth2/authorize?client_id=${this.secrets.twitch.client_id}&redirect_uri=${this.redirect_uri}&response_type=code&scope=chat:read chat:edit&force_verify=true`);
        }).get('/authorize/sub', (req, res) => {
            res.redirect(`https://id.twitch.tv/oauth2/authorize?client_id=${this.secrets.twitch.client_id}&redirect_uri=${this.redirect_uri}&response_type=code&scope=channel:read:subscriptions&force_verify=true`);
        }).get('/callback', (req, res) => {
            if (req.query.code) {
                this.webclient.post(`https://id.twitch.tv/oauth2/token?client_id=${this.secrets.twitch.client_id}&client_secret=${this.secrets.twitch.client_secret}&code=${req.query.code}&grant_type=authorization_code&redirect_uri=${this.redirect_uri}`)
                    .then((tokenRes) => {
                    let tokenData = {
                        expires_in: tokenRes.data.expires_in,
                        access_token: tokenRes.data.access_token,
                        refresh_token: tokenRes.data.refresh_token,
                        scope: tokenRes.data.scope
                    };
                    console.dir(tokenData);
                    if (tokenData.scope.includes('channel:read:subscriptions')) {
                        this.secrets.twitch.tokens.sub = tokenData;
                    }
                    else if (tokenData.scope.includes('chat:read')) {
                        this.secrets.twitch.tokens.chat = tokenData;
                    }
                    else {
                        console.error("Token scope not recognised");
                        res.status(500).send("Returned token not recognised. Please return to the original login link and try again.");
                        return;
                    }
                    fs.writeFile("lib/secrets.json", JSON.stringify(this.secrets, null, 4), (err) => {
                        if (err) {
                            console.error(`Unable to write token data to secret file: ${err}`);
                            res.status(500).send(`<p>The token was received, but an error occurred while saving it.</p><p>Please send Brad the following data:</p><p><pre>${tokenData}</pre></p>`);
                        }
                        else {
                            console.log("Token data written to secrets.json");
                            res.send("Thanks for logging in. The login has been saved for use with the bot. You can now close this tab/window.");
                        }
                    });
                }).catch(err => {
                    console.error(`Unable to retrieve token: ${err}`);
                    res.status(500).send("An error was returned getting the token. Please return to the original login link and try again.");
                });
            }
        }).listen(this.port, () => {
            console.log(`Notifier OAuth server listening on port ${this.port}`);
        });
    }
}
exports.default = new NotifierOauth();
//# sourceMappingURL=oauth.js.map