import fs = require('fs');
import TwitchAuth = require('twitch-auth');
class getAppToken {
    private secrets: Secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
    constructor() {
        TwitchAuth.getAppToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret).then((appToken: TwitchAuth.AccessToken) => {
            let diff: number = appToken.expiryDate.getTime() - Date.now();
            let expires_in: number = Math.abs(diff / 1000);
            this.secrets.twitch.tokens.app = {
                access_token: appToken.accessToken,
                expires_in: expires_in,
                refresh_token: appToken.refreshToken,
                scope: appToken.scope
            };
            fs.writeFileSync('lib/secrets.json', JSON.stringify(this.secrets));
        }).catch(err => {
            console.error(`Unable to get an app token: ${err}`);
        });
    }
}
export default new getAppToken();
interface Secrets {
    discord: {
        token: string;
    }
    twitch: {
        client_id: string;
        client_secret: string;
        tokens: {
            app: TwitchAuth.AccessTokenData;
            chat: TwitchAuth.AccessTokenData;
            sub?: TwitchAuth.AccessTokenData;
        }
    }
};
