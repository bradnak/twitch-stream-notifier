import Axios = require('axios');
import Express = require('express');
import fs = require('fs');
import TwitchAuth = require('twitch-auth')

class NotifierOauth {
    private secrets: Secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));
    private http: Express.Application = Express();
    private port: number = 8080;
    private redirect_uri: string = "http://ec2-54-153-178-105.ap-southeast-2.compute.amazonaws.com:8080/callback";
    private webclient: Axios.AxiosInstance = Axios.default.create();

    constructor() {
        this.http.get('/', (req: Express.Request, res: Express.Response) => {
            res.send("Welcome to Bradnak Stream Notifier. To login, visit /authorize/chat or /authorize/webhook");
        }).get('/authorize/chat', (req: Express.Request, res: Express.Response) => {
            res.redirect(`https://id.twitch.tv/oauth2/authorize?client_id=${this.secrets.twitch.client_id}&redirect_uri=${this.redirect_uri}&response_type=code&scope=chat:read chat:edit&force_verify=true`);
        }).get('/authorize/sub', (req: Express.Request, res: Express.Response) => {
            res.redirect(`https://id.twitch.tv/oauth2/authorize?client_id=${this.secrets.twitch.client_id}&redirect_uri=${this.redirect_uri}&response_type=code&scope=channel:read:subscriptions&force_verify=true`);
        }).get('/callback', (req: Express.Request, res: Express.Response) => {
            if (req.query.code) {
                this.webclient.post(`https://id.twitch.tv/oauth2/token?client_id=${this.secrets.twitch.client_id}&client_secret=${this.secrets.twitch.client_secret}&code=${req.query.code}&grant_type=authorization_code&redirect_uri=${this.redirect_uri}`)
                    .then((tokenRes: Axios.AxiosResponse<TokenResponse>) => {
                        let tokenData: TwitchAuth.AccessTokenData = {
                            expires_in: tokenRes.data.expires_in,
                            access_token: tokenRes.data.access_token,
                            refresh_token: tokenRes.data.refresh_token,
                            scope: tokenRes.data.scope
                        };
                        console.dir(tokenData);
                        if (tokenData.scope.includes('channel:read:subscriptions')) {
                            this.secrets.twitch.tokens.sub = tokenData;
                        }
                        else if (tokenData.scope.includes('chat:read')) {
                            this.secrets.twitch.tokens.chat = tokenData;
                        }
                        else {
                            console.error("Token scope not recognised");
                            res.status(500).send("Returned token not recognised. Please return to the original login link and try again.");
                            return;
                        }
                        fs.writeFile("lib/secrets.json", JSON.stringify(this.secrets, null, 4), (err: NodeJS.ErrnoException) => {
                            if (err) {
                                console.error(`Unable to write token data to secret file: ${err}`);
                                res.status(500).send(`<p>The token was received, but an error occurred while saving it.</p><p>Please send Brad the following data:</p><p><pre>${tokenData}</pre></p>`);
                            }
                            else {
                                console.log("Token data written to secrets.json");
                                res.send("Thanks for logging in. The login has been saved for use with the bot. You can now close this tab/window.");
                            }
                        });
                    }).catch(err => {
                        console.error(`Unable to retrieve token: ${err}`);
                        res.status(500).send("An error was returned getting the token. Please return to the original login link and try again.");
                    });
            }
        }).listen(this.port, () => {
            console.log(`Notifier OAuth server listening on port ${this.port}`);
        });
    }
}
export default new NotifierOauth();
interface Secrets {
    discord: {
        token: string;
    };
    twitch: {
        client_id: string;
        client_secret: string;
        tokens: {
            app: TwitchAuth.AccessToken;
            chat: TwitchAuth.AccessTokenData;
            sub?: TwitchAuth.AccessTokenData;
        }
    }
}
interface TokenResponse {
    access_token: string;
    refresh_token: string;
    expires_in: number;
    scope: string[];
    token_type: "bearer";
}