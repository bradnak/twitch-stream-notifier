import Discord = require('discord.js');
import fs = require('fs');

class MessageDeleter {
    private token: string = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8")).discord.token;
    private discord: Discord.Client = new Discord.Client();
    constructor() {
        this.discord.on("ready", () => {
            let channel: Discord.TextChannel = (this.discord.guilds.cache.get('93155159563075584').channels.cache.get('424717288860811265') as Discord.TextChannel);
            channel.messages.fetch().then((messages: Discord.Collection<string, Discord.Message>) => {
                messages.forEach((message: Discord.Message) => {
                    message.delete().then((deletedMsg: Discord.Message) => {
                        console.log(`Message deleted: ${deletedMsg.id}`);
                    }).catch(err => {

                    });
                });
            }).catch(err => {

            });
        });
    }
}
export default new MessageDeleter();