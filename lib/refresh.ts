import fs = require('fs');
import TwitchAuth = require('twitch-auth');

class RefreshTokens {
    private secrets: Secrets = JSON.parse(fs.readFileSync("lib/secrets.json", "utf-8"));

    constructor() {
        console.log("Refreshing Stream Notifier tokens...");
        TwitchAuth.refreshUserToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret, this.secrets.twitch.tokens.chat.refresh_token).then((chatToken: TwitchAuth.AccessToken) => {
            console.log("Chat token received");
            this.secrets.twitch.tokens.chat.access_token = chatToken.accessToken;
            this.secrets.twitch.tokens.chat.refresh_token = chatToken.refreshToken;
            this.secrets.twitch.tokens.chat.scope = chatToken.scope;
            console.log("Chat token saved");
            TwitchAuth.refreshUserToken(this.secrets.twitch.client_id, this.secrets.twitch.client_secret, this.secrets.twitch.tokens.sub.refresh_token).then((subToken: TwitchAuth.AccessToken) => {
                console.log("Sub token received");
                this.secrets.twitch.tokens.sub.access_token = subToken.accessToken;
                this.secrets.twitch.tokens.sub.refresh_token = subToken.refreshToken;
                this.secrets.twitch.tokens.sub.scope = chatToken.scope;
                console.log("Sub token saved");
                fs.writeFileSync("lib/secrets.json", JSON.stringify(this.secrets, null, 4));
            }).catch(err => {
                console.error(`Unable to refresh sub token: ${err}`);
            });
        }).catch(err => {
            console.error(`Unable to refresh chat token: ${err}`);
        });
    }
}

export default new RefreshTokens();

interface Secrets {
    discord: {
        token: string;
    };
    twitch: {
        client_id: string;
        client_secret: string;
        tokens: {
            app: TwitchAuth.AccessTokenData;
            chat: TwitchAuth.AccessTokenData;
            sub?: TwitchAuth.AccessTokenData;
        }
    }
}